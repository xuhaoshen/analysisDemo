package com.service2.strategy;

import java.util.Map;

/**
 * @author shenxuhao
 * @desc
 * @project samp
 * @date 2018/4/6 14:19
 */
public interface Strategy {

    Map<String, Object> consume();

    void send(Map<String, Object> map);
}
