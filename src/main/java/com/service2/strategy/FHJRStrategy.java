package com.service2.strategy;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author shenxuhao
 * @desc
 * @project samp
 * @date 2018/4/6 14:20
 */
@Service
public class FHJRStrategy implements Strategy {
    @Override
    public Map<String, Object> consume() {
        Map<String, Object> map = new HashMap<>();
        map.put("key", "value");
        System.out.println("消费数据");
        return map;
    }

    @Override
    public void send(Map<String, Object> map) {
        System.out.println(map.get("key"));
    }
}
