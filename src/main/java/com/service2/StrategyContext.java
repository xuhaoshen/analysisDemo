package com.service2;

import com.service2.strategy.Strategy;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * @author shenxuhao
 * @desc
 * @project samp
 * @date 2018/4/6 14:36
 */
public class StrategyContext {

    private Strategy strategy;

    @Autowired
    public StrategyContext(Strategy strategy) {
        this.strategy = strategy;
    }

    public void operate(String fundCode) {
        System.out.println("保存数据");
        Map<String, Object> map = strategy.consume();
        strategy.send(map);
    }
}
