package com.service2.impl;

import com.service2.StrategyContext;
import com.service2.FHJRTestService2;
import com.service2.strategy.FHJRStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author shenxuhao
 * @desc
 * @project samp
 * @date 2018/4/6 14:40
 */
@Service("fhjrTestService2")
public class FHJRTestService2Impl implements FHJRTestService2 {

    @Autowired
    private FHJRStrategy fhjrStrategy;

    @Override
    public void test() {
        StrategyContext strategyContext = new StrategyContext(fhjrStrategy);
        strategyContext.operate("FHJR");
    }
}
