package com.service1.impl;

import com.service1.FHJRTestService1;
import com.service1.rules.CommonFtpRules;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author shenxuhao
 * @desc
 * @project samp
 * @date 2018/4/6 10:36
 */
@Service
public class FHJRTestService1Impl implements FHJRTestService1 {

    @Autowired
    private CommonFtpRules commonFtpRules;
    @Override
    public void test() {
        commonFtpRules.operate("FFJR");
    }
}
