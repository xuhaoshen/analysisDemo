package com.service1.impl.rules;

import com.service1.rules.CommonFtpRules;
import com.service1.support.ConsumeSupport;
import com.service1.support.SaveSupport;
import com.service1.support.SendSupport;
import com.service1.support.Support;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author shenxuhao
 * @desc
 * @project samp
 * @date 2018/4/6 14:22
 */
@Service
public class CommonFtpRulesImpl implements CommonFtpRules {

    @Override
    public void operate(String fundCode) {
        System.out.println("操作的资金方是:" + fundCode);
        Map<String, Object> map = new HashMap<>();
        map.put("fundCode", fundCode);
        Support saveSupport = new SaveSupport();
        Support consumSupport = new ConsumeSupport();
        Support sendSupport = new SendSupport();
        saveSupport.setNext(consumSupport).setNext(sendSupport);
        saveSupport.support(map);
    }
}
