package com.service1.support;

import java.util.HashMap;
import java.util.Map;

/**
 * @author shenxuhao
 * @desc
 * @project samp
 * @date 2018/4/6 10:34
 */
public class ConsumeSupport extends Support {

    @Override
    protected void resolve() {
        Map<String, Object> map = new HashMap<>();
        map.put("key", "value");
        super.setData(map);
        System.out.println("消费数据");
    }
}
