package com.service1.support;

import java.util.Map;

/**
 * @author shenxuhao
 * @desc
 * @project samp
 * @date 2018/4/6 10:48
 */
public class SendSupport extends Support {

    @Override
    protected void resolve() {
        Map<String, Object> map1 = this.getPerious().getData();
        Map<String, Object> map2 = this.getPerious().getPerious().getData();
        System.out.println(map1.get("key"));
        System.out.println(map2.get("key"));
    }
}
