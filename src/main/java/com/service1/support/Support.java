package com.service1.support;

import java.util.Map;

/**
 * @author shenxuhao
 * @desc
 * @project samp
 * @date 2018/4/6 10:17
 */
public abstract class Support {

    private Support next;
    private Support perious;
    private Map<String, Object> data;

    public Support setNext(Support support) {
        support.setPerious(this);
        this.next = support;
        return next;
    }

    public final void support(Map<String, Object> map) {
        this.resolve();
        if(next != null) {
            next.support(map);
        }
    }

    protected abstract void resolve();

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public Support getPerious() {
        return perious;
    }

    public void setPerious(Support perious) {
        this.perious = perious;
    }
}
