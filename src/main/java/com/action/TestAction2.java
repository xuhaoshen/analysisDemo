package com.action;

import com.service2.FHJRTestService2;
import com.service2.impl.FHJRTestService2Impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shenxuhao
 * @desc
 * @project samp
 * @date 2018/4/6 14:41
 */
@RestController
public class TestAction2 {

    @Autowired
    private FHJRTestService2 fhjrTestService2;

    @RequestMapping("/test2")
    public void test() {
        fhjrTestService2.test();
    }
}
