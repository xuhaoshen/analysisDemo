package com.action;

import com.service1.FHJRTestService1;
import com.service1.impl.FHJRTestService1Impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shenxuhao
 * @desc
 * @project samp
 * @date 2018/4/6 10:40
 */
@RestController
public class TestAction1 {

    @Autowired
    private FHJRTestService1 fhjrTestService1;

    @RequestMapping("/test1")
    public void test() {
        fhjrTestService1.test();
    }
}
