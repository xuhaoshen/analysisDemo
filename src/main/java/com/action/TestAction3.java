package com.action;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Properties;

/**
 * @author shenxuhao
 * @desc
 * @project samp
 * @date 2018/4/7 9:37
 */
@RestController
public class TestAction3 {

    @RequestMapping("mail")
    public void test() throws Exception {
        Properties props = new Properties();                // 用于连接邮件服务器的参数配置（发送邮件时才需要用到）
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.qq.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.user", "2544793677@qq.com");
        props.put("mail.password", "tqwcgzrgdoidebfg");
        Authenticator authenticator = new Authenticator() {

            protected PasswordAuthentication getPasswordAuthentication() {
                // 用户名、密码
                String userName = props.getProperty("mail.user");
                String password = props.getProperty("mail.password");
                return new PasswordAuthentication(userName, password);
            }
        };

        Session session= Session.getInstance(props, authenticator);        // 根据参数配置，创建会话对象（为了发送邮件准备的）
        MimeMessage message = new MimeMessage(session);     // 创建邮件对象

        InternetAddress form = new InternetAddress(
                props.getProperty("mail.user"));
        message.setFrom(form);

        InternetAddress to = new InternetAddress("714363240@qq.com");
        message.setRecipient(MimeMessage.RecipientType.TO, to);

        message.setSubject("TEST邮件主题", "UTF-8");

        message.setContent("这是一份由皓爷用javax.mail发送的邮件。。。", "text/html;charset=UTF-8");

        message.setSentDate(new Date());

        Transport.send(message);
    }
}
